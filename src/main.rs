// simple wget clone, used to learn something
// about managing errors in rust
#![feature(slicing_syntax)]

extern crate argparse;
extern crate http;
extern crate url;

use std::{os, error};
use std::io::{File, IoError};
use argparse::{ArgumentParser, StoreOption};
use url::{Url, ParseError};
use http::client::RequestWriter;
use http::method::Get;

#[deriving(Show)]
enum RugetError {
    UrlError(ParseError),
    HttpError(IoError),
    FileError(IoError),
}

impl error::FromError<ParseError> for RugetError {
    fn from_error(err: ParseError) -> RugetError {
        UrlError(err)
    }
}
impl error::FromError<(RequestWriter, IoError)> for RugetError {
    fn from_error(err: (RequestWriter, IoError)) -> RugetError {
        HttpError(err.val1())
    }
}
impl error::FromError<IoError> for RugetError {
    fn from_error(err: IoError) -> RugetError {
        FileError(err)
    }
}

type RugetResult<T> = Result<T, RugetError>;

struct Args {
    url: Option<String>,
    dest: Option<String>,
}

fn url_retrieve(url : String, dest : String) -> RugetResult<()> {
    let url = try!(Url::parse(url[]));
    let request: RequestWriter = try!(RequestWriter::new(Get, url));
    let mut response = try!(request.read_response());
    let body = try!(response.read_to_end());
    let mut file = try!(File::create(&Path::new(dest[])));
    file.write(body[]);
    Ok(())
}

fn main() {
    let mut args = Args {
                            url: None,
                            dest: None,
                        };

    let mut parser = ArgumentParser::new();
    parser.set_description("minimal wget");
    parser.refer(&mut args.url)
        .add_argument("url", box StoreOption::<String>, "url to get")
        .required();
    parser.refer(&mut args.dest)
        .add_argument("dest", box StoreOption::<String>, "destination file")
        .required();
    match parser.parse_args() {
        Ok(()) => {}
        Err(x) => {
            os::set_exit_status(x);
            return;
        }
    }

    println!("url_retrieve {} -> {}", args.url, args.dest);
    match url_retrieve(args.url.unwrap(), args.dest.unwrap()) {
        Ok(()) => println!("Done!"),
        Err(why) => println!("Error {}", why),
    }
}

