#!/usr/bin/env python3
# encoding: utf-8

import os
import sys
import stat
import argparse
import logging
import urllib.request
from urllib.error import URLError

def parse_args():
    parser = argparse.ArgumentParser('minimal wget')
    parser.add_argument('url', help='url to get')
    parser.add_argument('dest', help='destination file')

    return parser.parse_args()

def url_retrieve(url, dest):
    with urllib.request.urlopen(url) as s:
        data = s.read().decode('utf-8')
        with open(dest, 'w+') as f:
            f.write(data)

def main(args):
    try:
        url_retrieve(args.url, args.dest)
        return 0
    except URLError as err:
        logging.error(err)
        return 1
    except IOError as err:
        logging.error(err)
        return 2

if __name__ == '__main__':
    args = parse_args()
    sys.exit(main(args))
